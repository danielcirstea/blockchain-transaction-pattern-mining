# Blockchain transaction pattern mining

Repository for blockchain transaction pattern mining framework.

## Description

Contains the main implementation of the algorithm featuring pq-grams, mined blockchain data, as well as various visual representations of results.

## License

TBD
