
from pq_profile import Profile
import csv

with open('data/graph1.csv') as f:  # change 'data/graph1.csv' to whatever input is needed
    data = [(line['from'], line['to']) for line in csv.DictReader(f, skipinitialspace=True)][:50]  # limit records here
    result = list(zip(*data))


def mine_patterns(p, q, t): # pattern mining
    profile = Profile(result, p, q, t)
    patterns = profile.pq_gram_profile()
    return patterns


def pq_gram_distance(profile1, profile2): # tree edit distance
    intersections, unions = 0, len(profile1) * 2
    for k in profile1:
        if k in profile2:
            intersections += 1
    return 1 - 2 * (intersections / unions)


print(mine_patterns(2, 3, 0)) # example call
